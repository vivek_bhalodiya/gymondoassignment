﻿# Gymondo Assessment - Vivek Bhalodiya

Android app that displays different exercises. 

# Screenshots

![Phone](https://drive.google.com/uc?export=view&id=13sQHCQ7pFuEBxf0BjN1OU0oVhATeR567)
![Tablet](https://drive.google.com/uc?export=view&id=1FSu2U_GW8_cGepnkfnxWEH5HLrdSzwi4)
## Requirements

    minSdkVersion 21
## Features

 - Ability to display all exercises
 - View Exercise info
 - Search exercise by name
 - Filter exercise by body part
 - Display Error state and loading indicator
 - All the lists are paginated

## Installation
There is no special requirements for the installation. You can install the App by opening the project in Android studio and make sure that you are on the **master branch**. 
Once project is loaded connect your android device and hit the Run button.

Alternatively you can i**nstall the apk** provided in the email.

## My approach for the APIs
Keeping in mind that  https://wger.de/api/v2/ has separate endpoints for getting exercise images, muscles, equipments and so on. 

For the User story 1, which says display all exercises with image, targeted muscles, and equipments. I have used **OPTIONS** for the **/exercise endpoint** which returns all the possible key value pairs for targeted muscles and equipments.

By doing this we can save unnecessary api calls when resolving keys for the same. Which means for each exercise item, I don't have to make separate api call to get the targeted muscle name by id. 

## Libraries used

 - Coroutine
 - Dagger 2
 - Paging library
 - Navigation Component
 - Retrofit
 - Glide
 - Material Components

## Notes
I was unable to write documentation in the project because of the time constraint and the api was down for a temporary period.

## Known issue
After searching exercises, tapping on **all** filter is not working. Please select different a filter and then choose filter **all**.
