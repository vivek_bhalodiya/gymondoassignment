package com.vivekbhalodiya.gymondoassesment.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
abstract class BaseFragment<B : ViewDataBinding, VM : ViewModel> : DaggerFragment() {
  @Inject
  lateinit var viewModelFactory: ViewModelProvider.Factory

  lateinit var viewModel: VM
  lateinit var binding: B

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    binding = DataBindingUtil.inflate(inflater, layoutId(), container, false)
    bindContentView()
    return binding.root
  }

  private fun bindContentView() {
    viewModel = ViewModelProvider(getActivityViewModelOwner(), viewModelFactory)
        .get(getViewModelClass())
  }

  abstract fun getViewModelClass(): Class<VM>

  abstract fun getActivityViewModelOwner(): ViewModelStoreOwner

  @LayoutRes
  protected abstract fun layoutId(): Int
}