package com.vivekbhalodiya.gymondoassesment.ui.base

import androidx.lifecycle.ViewModel

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
abstract class BaseViewModel : ViewModel() {
}