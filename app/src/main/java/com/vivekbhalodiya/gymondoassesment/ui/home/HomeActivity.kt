package com.vivekbhalodiya.gymondoassesment.ui.home

import android.os.Bundle
import com.vivekbhalodiya.gymondoassesment.R
import com.vivekbhalodiya.gymondoassesment.databinding.ActivityHomeBinding
import com.vivekbhalodiya.gymondoassesment.ui.base.BaseActivity

class HomeActivity : BaseActivity<ActivityHomeBinding, HomeActivityViewModel>() {

  override fun layoutId() = R.layout.activity_home

  override fun getViewModelClass() = HomeActivityViewModel::class.java

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    //Exercise fragment will be auto launched by the navigation component.
  }
}
