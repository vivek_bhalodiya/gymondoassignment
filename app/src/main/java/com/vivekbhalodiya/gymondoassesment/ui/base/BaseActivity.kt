package com.vivekbhalodiya.gymondoassesment.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
abstract class BaseActivity<B : ViewDataBinding, VM : ViewModel> : DaggerAppCompatActivity() {
  @Inject
  lateinit var viewModelFactory: ViewModelProvider.Factory

  lateinit var viewModel: VM
  lateinit var binding: B

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    bindContentView()
  }

  private fun bindContentView() {
    binding = DataBindingUtil.setContentView(this, layoutId())
    viewModel = ViewModelProvider(this, viewModelFactory)
        .get(getViewModelClass())
  }

  @LayoutRes
  protected abstract fun layoutId(): Int

  abstract fun getViewModelClass(): Class<VM>
}