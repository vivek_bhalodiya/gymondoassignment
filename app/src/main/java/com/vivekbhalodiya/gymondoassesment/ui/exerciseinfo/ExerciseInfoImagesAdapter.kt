package com.vivekbhalodiya.gymondoassesment.ui.exerciseinfo

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vivekbhalodiya.gymondoassesment.R
import com.vivekbhalodiya.gymondoassesment.data.models.ExerciseImages
import com.vivekbhalodiya.gymondoassesment.databinding.LayoutItemExerciseInfoImageBinding
import com.vivekbhalodiya.gymondoassesment.utils.setNetworkImage

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
class ExerciseInfoImagesAdapter(private val images: List<ExerciseImages.Result>) : RecyclerView.Adapter<ExerciseInfoImagesViewHolder>() {
  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): ExerciseInfoImagesViewHolder {
    return ExerciseInfoImagesViewHolder(
        DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_item_exercise_info_image, parent, false)
    )
  }

  override fun getItemCount(): Int {
    return images.size
  }

  override fun onBindViewHolder(
    holder: ExerciseInfoImagesViewHolder,
    position: Int
  ) {
    holder.bind(images[holder.adapterPosition])
  }

}

class ExerciseInfoImagesViewHolder(private val itemExerciseImage: LayoutItemExerciseInfoImageBinding) : RecyclerView.ViewHolder(
    itemExerciseImage.root
) {
  fun bind(image: ExerciseImages.Result) {
    image.image?.let {
      itemExerciseImage.exerciseImageView.setNetworkImage(it)
    }
  }
}
