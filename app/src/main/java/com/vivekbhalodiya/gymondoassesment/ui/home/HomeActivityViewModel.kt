package com.vivekbhalodiya.gymondoassesment.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.paging.PagedList.Config.Builder
import com.vivekbhalodiya.gymondoassesment.data.models.Exercise.Result
import com.vivekbhalodiya.gymondoassesment.data.repo.ExerciseRepository
import com.vivekbhalodiya.gymondoassesment.data.source.ExerciseDataSource
import com.vivekbhalodiya.gymondoassesment.data.source.ExerciseDataSourceFactory
import com.vivekbhalodiya.gymondoassesment.ui.base.BaseViewModel
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus
import javax.inject.Inject

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
class HomeActivityViewModel @Inject constructor() : BaseViewModel() {
  @Inject
  lateinit var exerciseRepository: ExerciseRepository

  var exerciseLiveData: LiveData<PagedList<Result>> = MutableLiveData()
  val paginationStatusLiveData = MutableLiveData<PaginationStatus>()
  lateinit var selectedExercise: Result

  private lateinit var exerciseDataSourceFactory: ExerciseDataSourceFactory
  private val pagedListConfig = Builder()
      .setInitialLoadSizeHint(ExerciseDataSource.EXERCISE_INIT_PAGE_SIZE)
      .setPageSize(ExerciseDataSource.EXERCISE_DEFAULT_PAGE_SIZE)
      .build()

  fun getAllExercises() {
    exerciseDataSourceFactory = ExerciseDataSourceFactory(exerciseRepository, viewModelScope, paginationStatusLiveData)
    exerciseLiveData = LivePagedListBuilder(exerciseDataSourceFactory, pagedListConfig).build()
  }

  fun filterExercise(filter: String?) {
    exerciseDataSourceFactory.filterExercise(filter)
    exerciseDataSourceFactory.exerciseLiveDataSource.value?.invalidate()
  }

  fun searchExercise(searchInput: String?) {
    exerciseDataSourceFactory.searchExercise(searchInput)
    exerciseDataSourceFactory.exerciseLiveDataSource.value?.invalidate()
  }

  fun setCurrentExercise(exercise: Result) {
    this.selectedExercise = exercise
  }

  fun retry() {
    exerciseDataSourceFactory.exerciseLiveDataSource.value?.invalidate()
  }
}