package com.vivekbhalodiya.gymondoassesment.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.vivekbhalodiya.gymondoassesment.R
import com.vivekbhalodiya.gymondoassesment.ui.home.HomeActivity

class SplashActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_splash)
    startActivity(Intent(this, HomeActivity::class.java))
    finish()
  }
}
