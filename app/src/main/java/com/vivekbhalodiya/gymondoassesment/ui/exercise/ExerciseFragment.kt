package com.vivekbhalodiya.gymondoassesment.ui.exercise

import android.R.layout
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.miguelcatalan.materialsearchview.MaterialSearchView.OnQueryTextListener
import com.vivekbhalodiya.gymondoassesment.R
import com.vivekbhalodiya.gymondoassesment.data.models.Exercise.Result
import com.vivekbhalodiya.gymondoassesment.data.models.ExerciseFilterType
import com.vivekbhalodiya.gymondoassesment.data.models.exerciseFilterList
import com.vivekbhalodiya.gymondoassesment.databinding.FragmentExerciseBinding
import com.vivekbhalodiya.gymondoassesment.ui.base.BaseFragment
import com.vivekbhalodiya.gymondoassesment.ui.home.HomeActivity
import com.vivekbhalodiya.gymondoassesment.ui.home.HomeActivityViewModel
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus.CompletedAfter
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus.CompletedInitial
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus.Empty
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus.Error
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus.LoadingAfter
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus.LoadingInitial

class ExerciseFragment : BaseFragment<FragmentExerciseBinding, HomeActivityViewModel>(),
    OnExerciseClickCallback {
  private var fragmentView: View? = null
  private var exerciseAdapter: ExerciseAdapter? = null
  private var spinner: Spinner? = null
  private var isFromSearch = false

  override fun getViewModelClass(): Class<HomeActivityViewModel> = HomeActivityViewModel::class.java

  override fun getActivityViewModelOwner(): ViewModelStoreOwner = activity as HomeActivity

  override fun layoutId(): Int = R.layout.fragment_exercise

  override fun onExerciseClick(exercise: Result) {
    viewModel.setCurrentExercise(exercise)
    navigateToExerciseInfoFragment()
  }

  override fun onCreateOptionsMenu(
    menu: Menu,
    inflater: MenuInflater
  ) {
    super.onCreateOptionsMenu(menu, inflater)
    inflater.inflate(R.menu.exercise_menu, menu)
    setupFilterSpinner(menu)
    setupSearchView(menu)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    if (fragmentView == null) {
      fragmentView = super.onCreateView(inflater, container, savedInstanceState)
      init()
    }
    return fragmentView
  }

  private fun init() {
    initToolbar()
    setupSearchViewQueryListener()
    initExerciseRecyclerView()
    getAllExercises()
    initObservers()
    initListeners()
  }

  private fun initToolbar() {
    (activity as HomeActivity).setSupportActionBar(this@ExerciseFragment.binding.exerciseToolbar.toolbarWithSearchAndFilter)
  }

  private fun setupSearchViewQueryListener() {
    binding.exerciseToolbar.searchView.setOnQueryTextListener(object : OnQueryTextListener {
      override fun onQueryTextSubmit(query: String): Boolean {
        viewModel.searchExercise(query)
        resetFilter()
        return false
      }

      override fun onQueryTextChange(newText: String): Boolean {
        return false
      }
    })
  }

  private fun initExerciseRecyclerView() {
    exerciseAdapter = ExerciseAdapter(this)
    binding.exerciseRecyclerView.adapter = exerciseAdapter
    val isPhone = resources.getBoolean(R.bool.is_phone)
    if (isPhone) {
      binding.exerciseRecyclerView.layoutManager = GridLayoutManager(requireContext(), 1)
    } else {
      binding.exerciseRecyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
    }
  }

  private fun getAllExercises() {
    viewModel.getAllExercises()
  }

  private fun initObservers() {
    viewModel.exerciseLiveData.observe(viewLifecycleOwner, Observer { exercises ->
      exercises?.let {
        exerciseAdapter?.submitList(exercises)
      }
    })

    viewModel.paginationStatusLiveData.observe(viewLifecycleOwner, Observer { paginationStatus ->
      paginationStatus?.let {
        handlePaginationStatus(it)
      }
    })
  }

  private fun initListeners() {
    binding.somethingWentWrong.retryButton.setOnClickListener {
      viewModel.retry()
    }
  }

  private fun handlePaginationStatus(status: PaginationStatus) {
    when (status) {
      LoadingInitial -> shouldShowProgress(true)
      LoadingAfter -> shouldShowLoadingAfterProgress(true)
      Empty -> showNoResultsFound(show = true)
      CompletedInitial -> shouldShowProgress(false)
      CompletedAfter -> shouldShowLoadingAfterProgress(false)
      is Error -> {
        showErrorState(show = true)
        showToast(getString(status.errorStringRes))
      }
    }
  }

  private fun navigateToExerciseInfoFragment() {
    closeSearchIfOpen()
    findNavController().navigate(R.id.action_exerciseFragment_to_exerciseInfoFragment)
  }

  private fun setupFilterSpinner(menu: Menu) {
    val itemFilter = menu.findItem(R.id.action_filter)
    spinner = itemFilter.actionView as Spinner
    val spinnerAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
        requireContext(),
        layout.simple_spinner_dropdown_item, exerciseFilterList
    )
    spinner?.onItemSelectedListener = onFilterItemOnClickListener()
    spinner?.adapter = spinnerAdapter
  }

  private fun setupSearchView(menu: Menu) {
    val item: MenuItem = menu.findItem(R.id.action_search)
    binding.exerciseToolbar.searchView.setMenuItem(item)
  }

  private fun onFilterItemOnClickListener(): OnItemSelectedListener {
    return object : OnItemSelectedListener {
      override fun onNothingSelected(p0: AdapterView<*>?) {

      }

      override fun onItemSelected(
        adapterView: AdapterView<*>?,
        p1: View?,
        p2: Int,
        p3: Long
      ) {
        if (isFromSearch) {
          isFromSearch = false
          return
        }

        onItemSelectedFilterExercise(adapterView)
      }

    }
  }

  private fun onItemSelectedFilterExercise(adapterView: AdapterView<*>?) {
    adapterView?.let {
      if (it.selectedItem is String) {
        ExerciseFilterType.values()
            .firstOrNull { item -> item.name == it.selectedItem }?.value?.let { value ->
          if (value.isEmpty())
            viewModel.filterExercise(null)
          else
            viewModel.filterExercise(value)
        }
      }
    }
  }

  private fun shouldShowProgress(show: Boolean) {
    showNoResultsFound(false)
    showErrorState(false)
    if (show) {
      binding.progressDialog.visibility = VISIBLE
    } else {
      binding.progressDialog.visibility = GONE
      shouldShowLoadingAfterProgress(false)
    }
  }

  private fun showErrorState(show: Boolean) {
    showNoResultsFound(false)
    if (show)
      binding.somethingWentWrong.root.visibility = VISIBLE
    else
      binding.somethingWentWrong.root.visibility = INVISIBLE
  }

  private fun shouldShowLoadingAfterProgress(show: Boolean) {
    showNoResultsFound(false)
    showErrorState(false)
    if (show) {
      if (binding.progressDialog.visibility == GONE)
        binding.horizontalProgressBar.visibility = VISIBLE
    } else {
      binding.horizontalProgressBar.visibility = INVISIBLE
    }
  }

  private fun showNoResultsFound(show: Boolean) {
    if (show) {
      if (binding.horizontalProgressBar.visibility == VISIBLE)
        binding.horizontalProgressBar.visibility = GONE
      binding.exerciseRecyclerView.visibility = INVISIBLE
      binding.noResultFoundTextView.visibility = VISIBLE
    } else {
      binding.exerciseRecyclerView.visibility = VISIBLE
      binding.noResultFoundTextView.visibility = GONE
    }
  }

  private fun showToast(message: String) {
    Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
        .show()
  }

  private fun resetFilter() {
    isFromSearch = true
    spinner?.setSelection(0)
  }

  private fun closeSearchIfOpen() {
    with(binding.exerciseToolbar.searchView) {
      if (isSearchOpen) {
        clearFocus()
        closeSearch()
      }
    }
  }
}
