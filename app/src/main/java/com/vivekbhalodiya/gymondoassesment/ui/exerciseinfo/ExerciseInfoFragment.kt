package com.vivekbhalodiya.gymondoassesment.ui.exerciseinfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.vivekbhalodiya.gymondoassesment.R
import com.vivekbhalodiya.gymondoassesment.databinding.FragmentExerciseInfoBinding
import com.vivekbhalodiya.gymondoassesment.ui.base.BaseFragment
import com.vivekbhalodiya.gymondoassesment.ui.home.HomeActivity
import com.vivekbhalodiya.gymondoassesment.ui.home.HomeActivityViewModel
import com.vivekbhalodiya.gymondoassesment.utils.setTextNotNull
import com.vivekbhalodiya.gymondoassesment.utils.toHTML

class ExerciseInfoFragment : BaseFragment<FragmentExerciseInfoBinding, HomeActivityViewModel>() {
  private var fragmentView: View? = null

  override fun getViewModelClass(): Class<HomeActivityViewModel> = HomeActivityViewModel::class.java

  override fun getActivityViewModelOwner(): ViewModelStoreOwner = activity as HomeActivity

  override fun layoutId(): Int = R.layout.fragment_exercise_info

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    if (fragmentView == null) {
      fragmentView = super.onCreateView(inflater, container, savedInstanceState)
      init()
    }
    return fragmentView
  }

  private fun init() {
    initToolbar()
    populateExerciseInfo()
    initExerciseImagesRecyclerView()
  }

  private fun initToolbar() {
    with(activity as HomeActivity) {
      setSupportActionBar(this@ExerciseInfoFragment.binding.toolbarExerciseInfo)
      supportActionBar?.setDisplayHomeAsUpEnabled(true)
      supportActionBar?.setDisplayShowHomeEnabled(true)
      this@ExerciseInfoFragment.binding.toolbarExerciseInfo.setNavigationOnClickListener {
        findNavController().popBackStack()
      }
    }
  }

  private fun populateExerciseInfo() {
    viewModel.selectedExercise.let {
      with(binding) {
        titleTextView.setTextNotNull(it.name ?: it.nameOriginal)
        categoryTextView.setTextNotNull(it.categoryName)
        equipmentsTextView.setTextNotNull(it.equipmentsNames)
        musclesTargetTextView.setTextNotNull(it.musclesNames)
        descriptionTextView.text = it.description?.toHTML()
      }
    }
  }

  private fun initExerciseImagesRecyclerView() {
    val images = viewModel.selectedExercise.images
    if (images.isNullOrEmpty()) {
      binding.exerciseImagesRecyclerView.visibility = GONE
      binding.exerciseImageView.visibility = VISIBLE
      return
    }
    binding.exerciseImagesRecyclerView.adapter = ExerciseInfoImagesAdapter(images)

    val isPhone = resources.getBoolean(R.bool.is_phone)
    if (isPhone) {
      binding.exerciseImagesRecyclerView.layoutManager = GridLayoutManager(requireContext(), 1, GridLayoutManager.HORIZONTAL, false)
    } else {
      binding.exerciseImagesRecyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
    }
  }
}
