package com.vivekbhalodiya.gymondoassesment.ui.exercise

import com.vivekbhalodiya.gymondoassesment.data.models.Exercise

/**
 * Created by Vivek Bhalodiya on 06/05/20.
 */
interface OnExerciseClickCallback {
  fun onExerciseClick(exercise: Exercise.Result)
}