package com.vivekbhalodiya.gymondoassesment.ui.exercise

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.vivekbhalodiya.gymondoassesment.R.layout
import com.vivekbhalodiya.gymondoassesment.data.models.Exercise
import com.vivekbhalodiya.gymondoassesment.data.models.Exercise.Result
import com.vivekbhalodiya.gymondoassesment.databinding.LayoutItemExerciseBinding
import com.vivekbhalodiya.gymondoassesment.utils.setNetworkImage
import com.vivekbhalodiya.gymondoassesment.utils.setTextNotNull

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
class ExerciseAdapter(private val onExerciseClickCallback: OnExerciseClickCallback) : PagedListAdapter<Exercise.Result, ExerciseViewHolder>(
    DIFF_CALLBACK
) {
  companion object {
    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Exercise.Result>() {
      override fun areItemsTheSame(
        oldItem: Result,
        newItem: Result
      ): Boolean = oldItem.id == newItem.id

      override fun areContentsTheSame(
        oldItem: Result,
        newItem: Result
      ): Boolean = oldItem == newItem
    }
  }

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): ExerciseViewHolder {
    return ExerciseViewHolder(
        DataBindingUtil.inflate(LayoutInflater.from(parent.context), layout.layout_item_exercise, parent, false), onExerciseClickCallback
    )
  }

  override fun onBindViewHolder(
    holder: ExerciseViewHolder,
    position: Int
  ) {
    getItem(holder.adapterPosition)?.let {
      holder.bind(it)
    }
  }
}

class ExerciseViewHolder(
  private val itemExercise: LayoutItemExerciseBinding,
  private val onExerciseClickCallback: OnExerciseClickCallback
) : RecyclerView.ViewHolder(itemExercise.root) {
  fun bind(exercise: Exercise.Result) {
    with(itemExercise) {
      categoryTextView.setTextNotNull(exercise.categoryName)
      titleTextView.setTextNotNull(exercise.name ?: exercise.nameOriginal)
      musclesTargetTextView.setTextNotNull(exercise.musclesNames)
      equipmentsTextView.setTextNotNull(exercise.equipmentsNames)
      if (exercise.images.isNullOrEmpty()
              .not()
      ) {
        exerciseImageView.setNetworkImage(exercise.images!!.first().image ?: "")
      }
      root.setOnClickListener {
        onExerciseClickCallback.onExerciseClick(exercise)
      }
    }
  }
}
