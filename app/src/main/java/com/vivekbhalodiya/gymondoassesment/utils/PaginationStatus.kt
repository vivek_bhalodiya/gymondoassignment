package com.vivekbhalodiya.gymondoassesment.utils

/**
 * Created by Vivek Bhalodiya on 07/05/20.
 */
sealed class PaginationStatus {
  object LoadingInitial : PaginationStatus()
  object LoadingAfter : PaginationStatus()
  object Empty : PaginationStatus()
  object CompletedInitial : PaginationStatus()
  object CompletedAfter : PaginationStatus()
  data class Error(val errorStringRes: Int) : PaginationStatus()
}