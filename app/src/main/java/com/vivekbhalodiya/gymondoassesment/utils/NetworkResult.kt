package com.vivekbhalodiya.gymondoassesment.utils

import retrofit2.Response

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
sealed class NetworkResult<out T> {
  data class Success<T>(val body: T) : NetworkResult<T>()
  data class Failure<T>(val errorResponse: Response<T>? = null) : NetworkResult<T>()
  data class IOFailure<T>(val errorString: String) : NetworkResult<T>()
  data class Exception<T>(val exception: kotlin.Exception) : NetworkResult<T>()
}