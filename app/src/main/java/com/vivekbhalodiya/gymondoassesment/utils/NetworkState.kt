package com.vivekbhalodiya.gymondoassesment.utils

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
enum class NetworkState {
  INIT,
  LOADING,
  SUCCESS,
  FAILED,
  NO_INTERNET
}