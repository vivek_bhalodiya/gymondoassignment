package com.vivekbhalodiya.gymondoassesment.utils

import android.content.SharedPreferences
import javax.inject.Inject

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
class PrefUtils @Inject constructor(private val prefs: SharedPreferences) {
  companion object {
    private const val EXERCISE_OPTION_KEY = "EXERCISE_OPTIONS"
  }

  var exerciseOptions: String?
    set(value) {
      prefs.edit()
          .putString(EXERCISE_OPTION_KEY, value)
          .apply()
    }
    get() = prefs.getString(EXERCISE_OPTION_KEY, null)
}