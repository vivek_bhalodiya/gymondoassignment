package com.vivekbhalodiya.gymondoassesment.utils

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.vivekbhalodiya.gymondoassesment.R

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */

fun TextView.setTextNotNull(text: String?) = if (text.isNullOrEmpty()) this.text = "N/A" else this.text = text

fun ImageView.setNetworkImage(url: String) {
  Glide.with(this)
      .load(url)
      .placeholder(R.drawable.ic_placeholder_broken_heart)
      .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
      .thumbnail()
      .fitCenter()
      .into(this)
}

fun String?.toHTML(): Spanned? {
  if (this == null) return null
  return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
    Html.fromHtml(this, Html.FROM_HTML_MODE_COMPACT)
  } else {
    Html.fromHtml(this)
  }
}