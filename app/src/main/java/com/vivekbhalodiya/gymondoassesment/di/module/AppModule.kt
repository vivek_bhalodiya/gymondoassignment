package com.vivekbhalodiya.gymondoassesment.di.module

import com.vivekbhalodiya.gymondoassesment.data.api.ApiService
import com.vivekbhalodiya.gymondoassesment.data.repo.ExerciseRepository
import com.vivekbhalodiya.gymondoassesment.utils.PrefUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
@Module
class AppModule {

  @Provides
  @Singleton internal fun provideExerciseRepository(
    apiService: ApiService,
    prefUtils: PrefUtils
  ): ExerciseRepository {
    return ExerciseRepository(apiService, prefUtils)
  }
}
