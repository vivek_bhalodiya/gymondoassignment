package com.vivekbhalodiya.gymondoassesment.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vivekbhalodiya.gymondoassesment.di.scope.ViewModelScope
import com.vivekbhalodiya.gymondoassesment.ui.home.HomeActivityViewModel
import com.vivekbhalodiya.gymondoassesment.utils.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
@Module
abstract class ViewModelFactoryModule {

  @Binds
  @IntoMap
  @ViewModelScope(HomeActivityViewModel::class)
  abstract fun bindHomeActivityViewModel(homeActivityViewModel: HomeActivityViewModel): ViewModel

  @Binds
  internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}