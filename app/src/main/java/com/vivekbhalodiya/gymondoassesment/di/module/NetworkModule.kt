package com.vivekbhalodiya.gymondoassesment.di.module

import com.vivekbhalodiya.gymondoassesment.BuildConfig
import com.vivekbhalodiya.gymondoassesment.data.api.ApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
@Module
class NetworkModule {
  @Provides
  @Singleton
  internal fun provideRetrofit(): Retrofit {
    val logging = HttpLoggingInterceptor()
    logging.level = BODY
    val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.MINUTES)
        .writeTimeout(1, TimeUnit.MINUTES)
        .readTimeout(1, TimeUnit.MINUTES)
        .addInterceptor(logging)
        .addInterceptor {
          val builder = it.request()
              .newBuilder()
              .addHeader("Authorization", BuildConfig.TOKEN)
              .method(it.request().method, it.request().body)
              .build()
          return@addInterceptor it.proceed(builder)
        }
        .build()

    return Retrofit.Builder()
        .client(client)
        .baseUrl(BuildConfig.BASE_URL)
        .callbackExecutor(Executors.newSingleThreadExecutor())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
  }

  @Provides
  @Singleton
  internal fun provideApiService(retrofit: Retrofit): ApiService {
    return retrofit.create(ApiService::class.java)
  }
}