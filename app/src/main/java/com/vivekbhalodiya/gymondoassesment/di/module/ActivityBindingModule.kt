package com.vivekbhalodiya.gymondoassesment.di.module

import androidx.fragment.app.FragmentManager
import com.vivekbhalodiya.gymondoassesment.di.scope.ActivityScope
import com.vivekbhalodiya.gymondoassesment.ui.home.HomeActivity
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.android.support.DaggerAppCompatActivity

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
@Module
abstract class ActivityBindingModule {

  @ActivityScope
  @ContributesAndroidInjector(modules = [BaseActivityModule::class])
  internal abstract fun bindHomeActivity(): HomeActivity
}

@Module(includes = [BaseActivityModule::class])
abstract class ActivityModule<in T : DaggerAppCompatActivity> {
  @Binds
  @ActivityScope
  internal abstract fun bindActivity(activity: T): DaggerAppCompatActivity
}

/**
 * Activity specific common dependencies should be placed here
 */
@Module
open class BaseActivityModule {

  @Provides
  @ActivityScope
  fun provideFragmentManager(activity: DaggerAppCompatActivity): FragmentManager = activity.supportFragmentManager
}