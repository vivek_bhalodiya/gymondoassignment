package com.vivekbhalodiya.gymondoassesment.di.module

import com.vivekbhalodiya.gymondoassesment.di.scope.FragmentScope
import com.vivekbhalodiya.gymondoassesment.ui.exercise.ExerciseFragment
import com.vivekbhalodiya.gymondoassesment.ui.exerciseinfo.ExerciseInfoFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.android.support.DaggerFragment

/**
 * Created by Vivek Bhalodiya on 06/05/20.
 */
@Module
abstract class FragmentBindingModule {
  @FragmentScope
  @ContributesAndroidInjector(modules = [(ExerciseInfoFragmentModule::class)])
  internal abstract fun showExerciseInfoFragment(): ExerciseInfoFragment

  @FragmentScope
  @ContributesAndroidInjector(modules = [(ExerciseFragmentModule::class)])
  internal abstract fun showExerciseFragment(): ExerciseFragment

  @Module
  internal abstract class ExerciseInfoFragmentModule : FragmentModule<ExerciseInfoFragment>()

  @Module
  internal abstract class ExerciseFragmentModule : FragmentModule<ExerciseFragment>()
}

@Module(includes = [BaseFragmentModule::class])
abstract class FragmentModule<in T : DaggerFragment> {
  @Binds
  @FragmentScope
  internal abstract fun bindFragment(fragment: T): DaggerFragment
}

/**
 * Fragment specific common dependencies should be placed here
 */
@Module
open class BaseFragmentModule