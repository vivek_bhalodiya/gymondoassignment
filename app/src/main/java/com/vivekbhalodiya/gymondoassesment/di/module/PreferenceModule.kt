package com.vivekbhalodiya.gymondoassesment.di.module

import android.content.Context
import android.preference.PreferenceManager
import com.vivekbhalodiya.gymondoassesment.di.qualifiers.ApplicationContext
import com.vivekbhalodiya.gymondoassesment.utils.PrefUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
@Module
class PreferenceModule {

  @Provides
  @Singleton
  internal fun provideSharedPreference(@ApplicationContext context: Context): PrefUtils {
    return PrefUtils(PreferenceManager.getDefaultSharedPreferences(context))
  }
}