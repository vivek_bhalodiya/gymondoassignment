package com.vivekbhalodiya.gymondoassesment.di.component

import android.content.Context
import com.vivekbhalodiya.gymondoassesment.GymondoApplication
import com.vivekbhalodiya.gymondoassesment.di.module.ActivityBindingModule
import com.vivekbhalodiya.gymondoassesment.di.module.AppModule
import com.vivekbhalodiya.gymondoassesment.di.module.FragmentBindingModule
import com.vivekbhalodiya.gymondoassesment.di.module.NetworkModule
import com.vivekbhalodiya.gymondoassesment.di.module.PreferenceModule
import com.vivekbhalodiya.gymondoassesment.di.module.ViewModelFactoryModule
import com.vivekbhalodiya.gymondoassesment.di.qualifiers.ApplicationContext
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
@Singleton
@Component(
    modules = [AppModule::class, NetworkModule::class, ViewModelFactoryModule::class, AndroidSupportInjectionModule::class, ActivityBindingModule::class, PreferenceModule::class, FragmentBindingModule::class]
)
interface AppComponent : AndroidInjector<GymondoApplication> {

  @Component.Builder
  abstract class Builder : AndroidInjector.Builder<GymondoApplication>() {
    @BindsInstance
    abstract fun appContext(@ApplicationContext context: Context)

    override fun seedInstance(instance: GymondoApplication?) {
      appContext(instance!!.applicationContext)
    }
  }
}