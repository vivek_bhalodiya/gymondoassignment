package com.vivekbhalodiya.gymondoassesment.di.qualifiers

import javax.inject.Qualifier

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
@Retention
@Qualifier
annotation class ApplicationContext
