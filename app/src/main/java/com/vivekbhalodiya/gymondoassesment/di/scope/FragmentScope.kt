package com.vivekbhalodiya.gymondoassesment.di.scope

import javax.inject.Scope

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
@Scope
@Retention
annotation class FragmentScope