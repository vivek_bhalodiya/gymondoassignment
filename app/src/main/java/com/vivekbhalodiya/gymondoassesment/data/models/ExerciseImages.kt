package com.vivekbhalodiya.gymondoassesment.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
data class ExerciseImages(
  @SerializedName("count")
  val count: Int?,
  @SerializedName("next")
  val next: Any?,
  @SerializedName("previous")
  val previous: Any?,
  @SerializedName("results")
  val results: List<Result>?
) {
  data class Result(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("license_author")
    val licenseAuthor: String?,
    @SerializedName("status")
    val status: String?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("is_main")
    val isMain: Boolean?,
    @SerializedName("license")
    val license: Int?,
    @SerializedName("exercise")
    val exercise: Int?
  )
}