package com.vivekbhalodiya.gymondoassesment.data.models

/**
 * Created by Vivek Bhalodiya on 07/05/20.
 */
enum class ExerciseFilterType(val value: String) {
  All(""),
  Abs("10"),
  Arms("8"),
  Back("12"),
  Calves("14"),
  Chest("11"),
  Legs("9"),
  Shoulders("13")
}

val exerciseFilterList: List<String>
  get() = ExerciseFilterType.values()
      .map { it.name }
