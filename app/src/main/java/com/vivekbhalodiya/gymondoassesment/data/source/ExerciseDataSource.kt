package com.vivekbhalodiya.gymondoassesment.data.source

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.vivekbhalodiya.gymondoassesment.BuildConfig
import com.vivekbhalodiya.gymondoassesment.R.string
import com.vivekbhalodiya.gymondoassesment.data.models.Exercise
import com.vivekbhalodiya.gymondoassesment.data.models.Exercise.Result
import com.vivekbhalodiya.gymondoassesment.data.repo.ExerciseRepository
import com.vivekbhalodiya.gymondoassesment.utils.NetworkResult
import com.vivekbhalodiya.gymondoassesment.utils.NetworkResult.Success
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber

/**
 * Created by Vivek Bhalodiya on 05/05/20.
 */
class ExerciseDataSource(
  private val exerciseRepository: ExerciseRepository,
  private val coroutineScope: CoroutineScope,
  private val searchInput: String?,
  private var filterInput: String?,
  private val paginationStatusLiveData: MutableLiveData<PaginationStatus>
) : PageKeyedDataSource<Int, Exercise.Result>() {

  companion object {
    const val EXERCISE_INIT_PAGE_SIZE = 8
    const val EXERCISE_DEFAULT_PAGE_SIZE = 5
  }

  private var nextPageKey: String? = null

  override fun loadInitial(
    params: LoadInitialParams<Int>,
    callback: LoadInitialCallback<Int, Exercise.Result>
  ) {
    paginationStatusLiveData.postValue(PaginationStatus.LoadingAfter)
    coroutineScope.launch {
      when (val result = exerciseRepository.getAllExercises(filterInput)) {
        is NetworkResult.Success -> {
          onLoadInitialSuccess(result, callback)
        }
        is NetworkResult.Failure -> {
          handleError(message = result.errorResponse?.message())
          Timber.e(result.errorResponse?.message())
        }
        is NetworkResult.IOFailure -> {
          handleError(message = result.errorString)
          Timber.e(result.errorString)
        }
        is NetworkResult.Exception -> {
          handleError(e = result.exception)
        }
      }
    }
  }

  private fun onLoadInitialSuccess(
    result: Success<Exercise>,
    callback: LoadInitialCallback<Int, Result>
  ) {
    val data = querySearchInput(result.body.results)
    nextPageKey = result.body.next
    if (nextPageKey == null || nextPageKey!!.isEmpty() || data == null) {
      callback.onResult(data ?: emptyList(), 0, null)
      if (data.isNullOrEmpty())
        paginationStatusLiveData.postValue(PaginationStatus.Empty)
    } else {
      callback.onResult(
          data, 0,
          EXERCISE_INIT_PAGE_SIZE
      )
      if (data.isNullOrEmpty())
        paginationStatusLiveData.postValue(PaginationStatus.Empty)
      else
        paginationStatusLiveData.postValue(PaginationStatus.CompletedInitial)
    }
  }

  override fun loadAfter(
    params: LoadParams<Int>,
    callback: LoadCallback<Int, Result>
  ) {
    paginationStatusLiveData.postValue(PaginationStatus.LoadingAfter)
    val nextPageNumberEndpoint = nextPageKey?.substringAfter(BuildConfig.BASE_URL, "")
    if (nextPageNumberEndpoint.isNullOrEmpty()) return
    coroutineScope.launch {
      when (val result = exerciseRepository.getExerciseFromNextPage(nextPageNumberEndpoint)) {
        is NetworkResult.Success -> {
          onLoadAfterSuccess(result, callback)
        }
        is NetworkResult.Failure -> {
          handleError(message = result.errorResponse?.message())
          Timber.e(result.errorResponse?.message())
        }
        is NetworkResult.IOFailure -> {
          handleError(message = result.errorString)
          Timber.e(result.errorString)
        }
        is NetworkResult.Exception -> {
          handleError(e = result.exception)
        }
      }
    }
  }

  private fun onLoadAfterSuccess(
    result: Success<Exercise>,
    callback: LoadCallback<Int, Result>
  ) {
    val data = querySearchInput(result.body.results)
    nextPageKey = result.body.next
    if (nextPageKey == null || nextPageKey!!.isEmpty() || data == null) {
      callback.onResult(data ?: emptyList(), null)
    } else {
      callback.onResult(data, 0)
      paginationStatusLiveData.postValue(PaginationStatus.CompletedAfter)
    }
  }

  override fun loadBefore(
    params: LoadParams<Int>,
    callback: LoadCallback<Int, Result?>
  ) {
  }

  private fun querySearchInput(results: MutableList<Result>?): MutableList<Result>? {
    if (searchInput == null)
      return results

    return results?.filter {
          if (it.name != null) it.name.contains(searchInput, true) else false
        }
        ?.toMutableList()
  }

  private fun handleError(
    e: Exception? = null,
    message: String? = null
  ) {
    paginationStatusLiveData.postValue(PaginationStatus.Error(string.please_check_your_internet_connection))
  }
}