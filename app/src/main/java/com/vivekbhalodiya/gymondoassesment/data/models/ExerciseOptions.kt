package com.vivekbhalodiya.gymondoassesment.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
data class ExerciseOptions(
  @SerializedName("name")
  val name: String?,
  @SerializedName("description")
  val description: String?,
  @SerializedName("renders")
  val renders: List<String?>?,
  @SerializedName("parses")
  val parses: List<String?>?,
  @SerializedName("actions")
  val actions: Actions?
) {
  data class Actions(
    @SerializedName("POST")
    val pOST: POST?
  ) {
    data class POST(
      @SerializedName("id")
      val id: Id?,
      @SerializedName("license_author")
      val licenseAuthor: LicenseAuthor?,
      @SerializedName("status")
      val status: Status?,
      @SerializedName("description")
      val description: Description?,
      @SerializedName("name")
      val name: Name?,
      @SerializedName("name_original")
      val nameOriginal: NameOriginal?,
      @SerializedName("creation_date")
      val creationDate: CreationDate?,
      @SerializedName("uuid")
      val uuid: Uuid?,
      @SerializedName("license")
      val license: License?,
      @SerializedName("category")
      val category: Category?,
      @SerializedName("language")
      val language: Language?,
      @SerializedName("muscles")
      val muscles: Muscles?,
      @SerializedName("muscles_secondary")
      val musclesSecondary: MusclesSecondary?,
      @SerializedName("equipment")
      val equipment: Equipment?
    ) {
      data class Id(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?
      )

      data class LicenseAuthor(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?,
        @SerializedName("help_text")
        val helpText: String?,
        @SerializedName("max_length")
        val maxLength: Int?
      )

      data class Status(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?
      )

      data class Description(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?
      )

      data class Name(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?,
        @SerializedName("max_length")
        val maxLength: Int?
      )

      data class NameOriginal(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?,
        @SerializedName("max_length")
        val maxLength: Int?
      )

      data class CreationDate(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?
      )

      data class Uuid(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?
      )

      data class License(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?,
        @SerializedName("choices")
        val choices: List<Choice?>?
      ) {
        data class Choice(
          @SerializedName("value")
          val value: String?,
          @SerializedName("display_name")
          val displayName: String?
        )
      }

      data class Category(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?,
        @SerializedName("choices")
        val choices: List<Choice?>?
      ) {
        data class Choice(
          @SerializedName("value")
          val value: String?,
          @SerializedName("display_name")
          val displayName: String?
        )
      }

      data class Language(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?,
        @SerializedName("choices")
        val choices: List<Choice?>?
      ) {
        data class Choice(
          @SerializedName("value")
          val value: String?,
          @SerializedName("display_name")
          val displayName: String?
        )
      }

      data class Muscles(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?,
        @SerializedName("choices")
        val choices: List<Choice?>?
      ) {
        data class Choice(
          @SerializedName("value")
          val value: String?,
          @SerializedName("display_name")
          val displayName: String?
        )
      }

      data class MusclesSecondary(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?,
        @SerializedName("choices")
        val choices: List<Choice?>?
      ) {
        data class Choice(
          @SerializedName("value")
          val value: String?,
          @SerializedName("display_name")
          val displayName: String?
        )
      }

      data class Equipment(
        @SerializedName("type")
        val type: String?,
        @SerializedName("required")
        val required: Boolean?,
        @SerializedName("read_only")
        val readOnly: Boolean?,
        @SerializedName("label")
        val label: String?,
        @SerializedName("choices")
        val choices: List<Choice?>?
      ) {
        data class Choice(
          @SerializedName("value")
          val value: String?,
          @SerializedName("display_name")
          val displayName: String?
        )
      }
    }
  }
}