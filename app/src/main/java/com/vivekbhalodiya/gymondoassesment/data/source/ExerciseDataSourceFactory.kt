package com.vivekbhalodiya.gymondoassesment.data.source

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.vivekbhalodiya.gymondoassesment.data.models.Exercise
import com.vivekbhalodiya.gymondoassesment.data.models.Exercise.Result
import com.vivekbhalodiya.gymondoassesment.data.repo.ExerciseRepository
import com.vivekbhalodiya.gymondoassesment.utils.PaginationStatus
import kotlinx.coroutines.CoroutineScope

/**
 * Created by Vivek Bhalodiya on 05/05/20.
 */
class ExerciseDataSourceFactory(
  private val exerciseRepository: ExerciseRepository,
  private val coroutineScope: CoroutineScope,
  private val paginationStatusLiveData: MutableLiveData<PaginationStatus>
) : DataSource.Factory<Int, Exercise.Result>() {
  val exerciseLiveDataSource: MutableLiveData<PageKeyedDataSource<Int, Exercise.Result>> = MutableLiveData()
  private var filter: String? = null
  private var searchInput: String? = null

  override fun create(): DataSource<Int, Result> {
    val exerciseDataSource = ExerciseDataSource(
        exerciseRepository = exerciseRepository, coroutineScope = coroutineScope, searchInput = searchInput, filterInput = filter,
        paginationStatusLiveData = paginationStatusLiveData
    )
    exerciseLiveDataSource.postValue(exerciseDataSource)
    return exerciseDataSource
  }

  fun filterExercise(filter: String?) {
    this.filter = filter
    this.searchInput = null
  }

  fun searchExercise(searchInput: String?) {
    this.searchInput = searchInput
    this.filter = null
  }
}