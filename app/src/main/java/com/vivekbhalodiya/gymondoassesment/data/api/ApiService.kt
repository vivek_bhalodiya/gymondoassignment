package com.vivekbhalodiya.gymondoassesment.data.api

import com.vivekbhalodiya.gymondoassesment.data.models.Exercise
import com.vivekbhalodiya.gymondoassesment.data.models.ExerciseImages
import com.vivekbhalodiya.gymondoassesment.data.models.ExerciseOptions
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.OPTIONS
import retrofit2.http.Query
import retrofit2.http.Url

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
interface ApiService {

  @GET("exercise/") suspend fun getExercises(
    @Query("status") status: Int? = null,
    @Query("page") page: String? = null,
    @Query("limit") limit: Int? = null,
    @Query("category") category: String? = null
  ): Response<Exercise>

  @GET
  suspend fun getExercisesFromNextPage(@Url url: String): Response<Exercise>

  @OPTIONS("exercise/") suspend fun getExerciseOptions(): Response<ExerciseOptions>

  @GET("exerciseimage/") suspend fun getExerciseImages(
    @Query("is_main") isMain: Boolean? = null,
    @Query("exercise") id: Int
  ): Response<ExerciseImages>
}