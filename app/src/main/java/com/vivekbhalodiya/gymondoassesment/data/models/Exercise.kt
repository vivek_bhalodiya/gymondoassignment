package com.vivekbhalodiya.gymondoassesment.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
data class Exercise(
  @SerializedName("count")
  val count: Int?,
  @SerializedName("next")
  val next: String?,
  @SerializedName("previous")
  val previous: Any?,
  @SerializedName("results")
  val results: MutableList<Result>?
) {
  data class Result(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("license_author")
    val licenseAuthor: String?,
    @SerializedName("status")
    val status: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("name_original")
    val nameOriginal: String?,
    @SerializedName("creation_date")
    val creationDate: String?,
    @SerializedName("uuid")
    val uuid: String?,
    @SerializedName("license")
    val license: Int?,
    @SerializedName("category")
    val category: Int?,
    var categoryName: String? = null,
    @SerializedName("language")
    val language: Int?,
    var languageName: String? = null,
    @SerializedName("muscles")
    val muscles: List<Int?>?,
    var musclesNames: String? = null,
    @SerializedName("muscles_secondary")
    val musclesSecondary: List<Int?>?,
    @SerializedName("equipment")
    val equipment: List<Int?>?,
    var equipmentsNames: String? = null,
    var images: List<ExerciseImages.Result>? = null
  )
}
