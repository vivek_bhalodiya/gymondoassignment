package com.vivekbhalodiya.gymondoassesment.data.repo

import com.google.gson.Gson
import com.vivekbhalodiya.gymondoassesment.data.api.ApiService
import com.vivekbhalodiya.gymondoassesment.data.models.Exercise
import com.vivekbhalodiya.gymondoassesment.data.models.ExerciseOptions
import com.vivekbhalodiya.gymondoassesment.utils.NetworkResult
import com.vivekbhalodiya.gymondoassesment.utils.PrefUtils
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flatMapMerge
import kotlinx.coroutines.flow.flow
import timber.log.Timber
import java.io.IOException

/**
 * Created by Vivek Bhalodiya on 04/05/20.
 */
class ExerciseRepository(
  private val apiService: ApiService,
  private val prefUtils: PrefUtils
) {

  companion object {
    //For verified exercises
    private const val EXERCISE_STATUS_CODE = 2
    private const val PER_PAGE_LIMIT = 8
  }

  suspend fun getAllExercises(filterInput: String?): NetworkResult<Exercise> {
    try {
      getExerciseOptions()
      val response = apiService.getExercises(status = EXERCISE_STATUS_CODE, limit = PER_PAGE_LIMIT, category = filterInput)
      if (response.isSuccessful) {
        var data = response.body()
        if (data != null) {
          if (data.results != null) {
            data = mapExerciseConstantsWithValues(data)
            data = mapExercisesImages(data)
          }
          return NetworkResult.Success(data)
        }
      }
      return NetworkResult.Failure(response)
    } catch (e: IOException) {
      return NetworkResult.IOFailure(e.localizedMessage!!)
    } catch (e: Exception) {
      return NetworkResult.Exception(e)
    }
  }

  suspend fun getExerciseFromNextPage(url: String): NetworkResult<Exercise> {
    try {
      getExerciseOptions()
      val response = apiService.getExercisesFromNextPage(url)
      if (response.isSuccessful) {
        var data = response.body()
        if (data != null) {
          data = mapExerciseConstantsWithValues(data)
          data = mapExercisesImages(data)
          return NetworkResult.Success(data)
        }
      }
      return NetworkResult.Failure(response)
    } catch (e: IOException) {
      return NetworkResult.IOFailure(e.localizedMessage!!)
    } catch (e: Exception) {
      return NetworkResult.Exception(e)
    }
  }

  private suspend fun mapExercisesImages(exercises: Exercise): Exercise {
    try {
      exercises.results?.asFlow()
          ?.flatMapMerge(concurrency = 20) { exercise ->
            flow {
              emit(exercise to apiService.getExerciseImages(id = exercise.id!!))
            }
          }
          ?.collect { response ->
            if (response.second.isSuccessful && response.second.body() != null && response.second.body()!!.results != null) {
              response.first.images = response.second.body()?.results!!
            }
          }
    } catch (e: Exception) {
      Timber.e(e)
    }

    return exercises
  }

  private suspend fun getExerciseOptions(): NetworkResult<Boolean> {
    try {
      if (prefUtils.exerciseOptions == null) {
        val response = apiService.getExerciseOptions()
        if (response.isSuccessful && response.body() != null) {
          saveExerciseOptionsToPrefs(response.body()!!)
          return NetworkResult.Success(true)
        }
        return NetworkResult.Failure()
      }
      return NetworkResult.Success(true)
    } catch (e: IOException) {
      return NetworkResult.IOFailure(e.localizedMessage!!)
    } catch (e: Exception) {
      return NetworkResult.Exception(e)
    }
  }

  private fun mapExerciseConstantsWithValues(exercise: Exercise): Exercise {
    val exerciseOptions = getExerciseOptionsFromPrefs()
    if (exerciseOptions == null || exercise.results == null)
      return exercise

    for (result in exercise.results) {
      if (result?.category != null)
        result.categoryName = resolveCategoryName(exerciseOptions, result.category)
      if (result?.equipment != null)
        result.equipmentsNames = resolveEquipmentsNames(exerciseOptions, result.equipment)
      if (result?.muscles != null)
        result.musclesNames = resolveMusclesNames(exerciseOptions, result.muscles)
    }

    return exercise
  }

  private fun resolveCategoryName(
    exerciseOptions: ExerciseOptions,
    category: Int
  ) = exerciseOptions.actions?.pOST?.category?.choices?.firstOrNull { category == it?.value?.toInt() }?.displayName

  private fun resolveMusclesNames(
    exerciseOptions: ExerciseOptions,
    muscles: List<Int?>
  ): String {
    val muscleNamesList = mutableListOf<String>()
    muscles.forEach { muscle ->
      exerciseOptions.actions?.pOST?.muscles?.choices?.firstOrNull { muscle == it?.value?.toInt() }
          ?.let {
            it.displayName?.let { muscleName ->
              muscleNamesList.add(muscleName)
            }
          }
    }

    return if (muscleNamesList.isEmpty()) "" else muscleNamesList.joinToString(separator = ", ")
  }

  private fun resolveEquipmentsNames(
    exerciseOptions: ExerciseOptions,
    equipments: List<Int?>
  ): String? {
    val equipmentsNamesList = mutableListOf<String>()
    equipments.forEach { equipment ->
      exerciseOptions.actions?.pOST?.equipment?.choices?.firstOrNull { equipment == it?.value?.toInt() }
          ?.let {
            it.displayName?.let { equipmentName ->
              equipmentsNamesList.add(equipmentName)
            }
          }
    }
    return if (equipmentsNamesList.isEmpty()) "" else equipmentsNamesList.joinToString(separator = ", ")
  }

  private fun saveExerciseOptionsToPrefs(exercise: ExerciseOptions) {
    prefUtils.exerciseOptions = Gson().toJson(exercise)
  }

  private fun getExerciseOptionsFromPrefs(): ExerciseOptions? {
    return Gson().fromJson(prefUtils.exerciseOptions ?: "", ExerciseOptions::class.java)
  }
}